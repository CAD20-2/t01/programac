#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

void escreverArquivo(char *nome, int n, int total){

    FILE *arquivoResultado;
    char textoResultado[20];
    char textoTemp[20];

    arquivoResultado = fopen(nome, "a");
    
    sprintf(textoResultado,"%d", n);
    strcat(textoResultado, ";");
    sprintf(textoTemp,"%d", total);
    strcat(textoResultado, textoTemp);
    strcat(textoResultado, "\n");
    fprintf(arquivoResultado, "%s", textoResultado);
    fclose(arquivoResultado);
}


int main(int argc, char *argv[]){

    int n = atoi(argv[1]);
    srand((double)(time(NULL)));
    double **matriz, *vetor, *produto;
    clock_t inicio, fim, total;

    produto = (double *)(malloc(n*sizeof(double)));
    matriz = (double **)(malloc(n*sizeof(double)));
    vetor = (double *)(malloc(n*sizeof(double)));
    matriz[0] = (double *)(malloc(n*n*sizeof(double)));
    
    for(int i = 1; i < n; i++) {
        matriz[i] = matriz[0] + i * n;
    }
    
    for(int linha = 0; linha < n; linha++){
        vetor[linha] = rand();
        for(int coluna = 0; coluna < n; coluna++){
            matriz[linha][coluna] = rand();
        }
    }

    inicio = clock();
    for(int linha = 0; linha < n; linha++){
        for(int coluna = 0; coluna < n; coluna++){
            produto[linha] = produto[linha] + matriz[linha][coluna] * vetor[coluna];
        }
    }
    fim = clock();
    total = (double)(fim - inicio) / CLOCKS_PER_SEC;
    escreverArquivo("resultadoLinhaColuna02.txt", n, total);

    inicio = clock();
    for(int coluna = 0; coluna < n; coluna++){
        for(int linha = 0; linha < n; linha++){
            produto[linha] = produto[linha] + matriz[linha][coluna] * vetor[coluna];
        }
    }
    fim = clock();
    total = (double)(fim - inicio) / CLOCKS_PER_SEC;
    escreverArquivo("resultadoColunaLinha02.txt", n, total);

    free(produto);
    free(matriz);
    free(vetor);
    return 0;
}