#%%
import os
# %%

# Bloco que realiza a chamada da função para diferentes n até nMax.
n = 100
nMax = 40000
incremento = 100
while n <= nMax:
    os.system("./programaC "+str(n))
    n+=incremento

#%%
import pandas as pd 
#%%
dict = {0: 'N', 1:'Tempo (s)'}
dfLinhaColuna = pd.read_csv('resultadoLinhaColuna02.txt', delimiter=';', header=None)
dfLinhaColuna.rename(columns = dict, inplace=True) 
dfLinhaColuna

#%%
dict = {0: 'N', 1:'Tempo (s)'}
dfColunaLinha = pd.read_csv('resultadoColunaLinha02.txt', delimiter=';', header=None)
dfColunaLinha.rename(columns = dict, inplace=True) 
dfColunaLinha
# %%
import matplotlib.pyplot as plt 
fig = plt.figure(figsize=(10,10))

plt.plot(dfLinhaColuna['Tempo (s)'], dfLinhaColuna['N'], label='Linha Coluna')
plt.plot(dfColunaLinha['Tempo (s)'], dfColunaLinha['N'], label='Coluna Linha')

plt.xlabel('Tempo (s)')
plt.ylabel('N')

plt.title("C - N x Tempo")

plt.legend()

plt.savefig('grafico.jpg')
plt.show()

